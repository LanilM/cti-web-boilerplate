#!/bin/bash
export $(grep -v '^#' .env | xargs)
envsubst < ./env.template.js > ./env.js

cat ./env.js
echo "Env Updated"
