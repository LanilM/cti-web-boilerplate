/* eslint-disable global-require */
const CracoLessPlugin = require('craco-less');
const COLOUR = require('./src/constants/colors');

module.exports = {
  plugins: [
    {
      plugin: CracoLessPlugin,
      options: {
        lessLoaderOptions: {
          lessOptions: {
            modifyVars: {
              '@primary-color': COLOUR.BLUE,
              '@success-color': COLOUR.GREEN,
              '@warning-color': COLOUR.YELLOW,
            },
            javascriptEnabled: true,
          },
        },
      },
    },
  ],
  style: {
    postcss: {
      plugins: [
        // eslint-disable-next-line import/no-unresolved
        require('tailwindcss'),
        require('autoprefixer'),
      ],
    },
  },
};
