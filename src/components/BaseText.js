import PropTypes from 'prop-types';

const BaseText = ({
  text,
}) => <span className="text-lg">{text}</span>;

BaseText.propTypes = {
  text: PropTypes.string.isRequired,
};

export default BaseText;
