import i18n from 'i18next';
import {
  initReactI18next,
} from 'react-i18next';
import en from './en';
import fr from './fr';
import languages from './languages';

i18n.use(initReactI18next)
  .init({
    resources: {
      en: {
        translation: en,
      },
      fr: {
        translation: fr,
      },
    },
    lng: languages.en,
    fallbackLng: languages.en,
    interpolation: {
      escapeValue: false,
    },
  });
