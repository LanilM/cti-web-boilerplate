import {
  clientsClaim,
} from 'workbox-core';
import {
  precacheAndRoute,
} from 'workbox-precaching';

clientsClaim();


// eslint-disable-next-line no-restricted-globals,no-underscore-dangle
precacheAndRoute(self.__WB_MANIFEST);
let key = null;


// eslint-disable-next-line no-restricted-globals
self.addEventListener('message', (event) => {
  if (event.data && event.data.type === 'SKIP_WAITING') {
    // eslint-disable-next-line no-restricted-globals
    self.skipWaiting();
  }
});


// eslint-disable-next-line no-restricted-globals
self.addEventListener('message', (event)=> {
  switch (event.data.type){
  case 'set':
    key = event.data.value;
    break;
  case 'get':
    event.ports[0].postMessage(key);
    break;
  default : break;

  }
});
