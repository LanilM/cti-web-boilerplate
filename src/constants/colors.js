module.exports = {
  BLUE: '#2980a6',
  LIGHT_BLUE: '#dff8ff',
  GRAY: '#f3f3f3',
  DARK_GRAY: '#cfcfcf',
  GREEN: '#118847',
  LIGHT_GREEN: '#e5f0e9',
  LIGHT_YELLOW: '#fef6dc',
  YELLOW: '#ffd440',
};
