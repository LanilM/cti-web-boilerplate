import {
  Layout as AntdLayout,
} from 'antd';

const {
  Footer,
} = AntdLayout;

const AppFooter = () => (
  <Footer className="footer px-8 md:px-24">
    <p>Footer</p>
  </Footer>
);

export default AppFooter;
