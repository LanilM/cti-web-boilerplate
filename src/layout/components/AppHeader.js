import {
  Layout as AntdLayout,
} from 'antd';

const {
  Header,
} = AntdLayout;

const AppHeader = () => (
  <Header className="header px-6 md:px-24">
    <p>Header</p>
  </Header>
);

export default AppHeader;
