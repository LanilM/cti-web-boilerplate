import {
  Suspense,
} from 'react';
import {
  Layout as AntdLayout,
} from 'antd';
import {
  Route, Switch,
} from 'react-router-dom';
import routes from '../routes/routes';
import AppHeader from './components/AppHeader';
import AppFooter from './components/AppFooter';

const {
  Content,
} = AntdLayout;

const routeSwitches = routes.map((route) => (
  <Route exact key={route.path} path={route.path}>
    {route.component}
  </Route>
));

const Layout = () => (
  <AntdLayout
    style={{
      minHeight: '100vh',
    }}
  >
    <AppHeader />
    <Content>
      <div>
        <Suspense fallback={<p>Loading ... ...</p>}>
          <Switch>{routeSwitches}</Switch>
        </Suspense>
      </div>
    </Content>
    <AppFooter />
  </AntdLayout>
);

export default Layout;
