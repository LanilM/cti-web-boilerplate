import {
  createSlice,
} from '@reduxjs/toolkit';

const layoutReducer = createSlice({
  name: 'layout',
  initialState: {
    test: true,
  },
  reducers: {
    setTest: (state) => {
      state.notificationModal = false;
    },
  },
});

export const {
  setTest,
} = layoutReducer.actions;

export default layoutReducer.reducer;
