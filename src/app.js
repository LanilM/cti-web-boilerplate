import './styles/app.less';
import {
  Provider as ReduxProvider,
} from 'react-redux';
import {
  BrowserRouter as Router,
} from 'react-router-dom';
import store from './state/store';
import './localization/i18n';
import Layout from './layout/Layout';

const App = () => (
  <ReduxProvider store={store}>
    <Router>
      <Layout />
    </Router>
  </ReduxProvider>
);

export default App;
