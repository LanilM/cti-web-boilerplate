import {
  combineReducers,
} from 'redux';
import layout from '../layout/state/layoutReducer';

export default combineReducers({
  layout,
});
