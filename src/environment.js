const environment = {
  REACT_APP_HEADER_NAME: window.env ? window.env.REACT_APP_HEADER_NAME : 'CTI_WEB_BOILERPLATE',
};

export default environment;
