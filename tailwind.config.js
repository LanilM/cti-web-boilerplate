const COLOUR = require('./src/constants/colors');

module.exports = {
  purge: {
    content: [ './src/**/*.{js,jsx,ts,tsx}', './public/index.html' ],
    safelist: [
      'border-green',
      'text-green',
      'bg-light-green',
      'border-yellow',
      'text-yellow',
      'bg-light-yellow',
    ],
  },
  darkMode: false, // or 'media' or 'class'
  theme: {
    extend: {},
    fontFamily: {
      sans: [ 'Open Sans', 'sans-serif' ],
      heading: [ 'Raleway', 'sans-serif' ],
    },
    fontWeight: {
      medium: 700,
    },
    colors: {
      blue: COLOUR.BLUE,
      'light-blue': COLOUR.LIGHT_BLUE,
      green: COLOUR.GREEN,
      'light-green': COLOUR.LIGHT_GREEN,
      yellow: COLOUR.YELLOW,
      'light-yellow': COLOUR.LIGHT_YELLOW,
    },
  },
  variants: {
    extend: {},
  },
  plugins: [],
};
