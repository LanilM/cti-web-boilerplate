module.exports = {
  env: {
    browser: true,
    es6: true,
  },
  parser: 'babel-eslint',
  ignorePatterns: [ 'node_modules/*', 'build/*' ],
  extends: [ 'plugin:react/recommended', 'airbnb', 'prettier' ],
  plugins: [ 'react', 'prettier', 'prefer-arrow' ],
  rules: {
    'react/require-default-props': [ 'off' ],
    'react/react-in-jsx-scope': [ 'off' ],
    'react/jsx-one-expression-per-line': [ 'off' ],
    'react/jsx-curly-newline': [ 'off' ],
    'react/state-in-constructor': 'off',
    'react/jsx-filename-extension': [
      'error',
      {
        extensions: [ '.js' ],
      },
    ],
    'jsx-a11y/click-events-have-key-events': [ 'off' ],
    'jsx-a11y/no-noninteractive-element-interactions': [ 'off' ],
    'jsx-a11y/label-has-associated-control': [ 'off' ],
    'jsx-a11y/no-static-element-interactions': [ 'off' ],
    'import/no-extraneous-dependencies': [
      'error',
      {
        devDependencies: true,
      },
    ],
    'no-undef': [ 'off' ],
    'object-curly-newline': [
      'error',
      {
        minProperties: 1,
      },
    ],
    'object-property-newline': [
      'error',
      {
        allowAllPropertiesOnSameLine: false,
      },
    ],
    quotes: [
      'error',
      'single',
      {
        allowTemplateLiterals: true,
      },
    ],
    'eol-last': [ 'error' ],
    'object-curly-spacing': [ 'error', 'always' ],
    indent: [ 'error', 2 ],
    'react/jsx-indent': [ 'error', 2 ],
    'react/jsx-indent-props': [ 'error', 2 ],
    'keyword-spacing': [ 'error' ],
    'no-multi-spaces': [ 'error' ],
    'no-trailing-spaces': [ 'error' ],
    'react/jsx-space-before-closing': [ 'error' ],
    semi: [ 'error' ],
    'array-bracket-spacing': [ 'error', 'always' ],
    'space-infix-ops': [ 'error' ],
    'no-console': [
      'error',
      {
        allow: [ 'error' ],
      },
    ],
    'key-spacing': [ 'error' ],
    'comma-dangle': [ 'error' ],
    'comma-spacing': [ 'error' ],
    'arrow-body-style': [ 'error' ],
    'max-len': [
      'error',
      {
        code: 100,
        ignoreUrls: true,
      },
    ],
    'operator-linebreak': [
      'error',
      'after',
      {
        overrides: {
          '?': 'before',
          ':': 'before',
        },
      },
    ],
    'newline-per-chained-call': [
      'error',
      {
        ignoreChainWithDepth: 1,
      },
    ],
    'prefer-arrow/prefer-arrow-functions': [
      'warn',
      {
        disallowPrototype: true,
        singleReturnOnly: true,
        classPropertiesAllowed: false,
      },
    ],
    'quote-props': [ 'error', 'as-needed' ],
    'no-param-reassign': [
      'error',
      {
        props: true,
        ignorePropertyModificationsFor: [ 'state', 'window' ],
      },
    ],
    'react/jsx-pascal-case': [ 'error' ],
  },
};
