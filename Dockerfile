# Stage => Build container
FROM node:16.4.2-alpine3.12 as builder

# Add Working Directory for the App.
WORKDIR /app

# Ensure both package.json AND package-lock.json are copied
COPY package*.json ./

# Install Dependencies
RUN npm install

# Copy rest of the files
COPY . .

# Build the App
RUN npm run build

# --------------------------------------------------------

# Stage => Run container
FROM nginx:1.21.3-alpine

USER 0

# Nginx config
RUN rm -rf /etc/nginx/conf.d
COPY conf/nginx /etc/nginx

# Users are not allowed to listen on priviliged ports hence change port
RUN sed -i.bak 's/listen\(.*\)80;/listen 8080;/' /etc/nginx/conf.d/default.conf  && \
# Comment user directive as master process is run as user in OpenShift anyhow
    sed -i.bak 's/^user/#user/' /etc/nginx/nginx.conf

EXPOSE 8080

# Static build
COPY --from=builder /app/build /usr/share/nginx/html/

# Copy .env file and shell script to container
WORKDIR /usr/share/nginx/html
COPY env.* ./

RUN chgrp -R 0 /var/cache/nginx /var/run && \
    chmod -R g+rwX /var/cache/nginx /var/run && \
    chgrp 0 ./env.sh ./env.js && \
    chmod g=u+x ./env.sh ./env.js

# Start Nginx server
CMD ["/bin/sh", "-c", "sh /usr/share/nginx/html/env.sh && nginx -g \"daemon off;\""]
